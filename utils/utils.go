package utils

import "math/rand"

func ContainsString(arr []string, str string) bool {
	for _, val := range arr {
		if val == str {
			return true
		}
	}
	return false
}

func GetRandom(min int, max int) int {
	if min >= max {
		return max
	}
	max++
	return rand.Int()%(max-min) + min
}

func GetSmoothedRandom(min int, max int) int {
	num := 0
	const smoothing = 2
	for i := 0; i < smoothing; i++ {
		num += GetRandom(min, max)
	}
	num /= smoothing
	return num
}

func GetRandomString(arr []string) string {
	return arr[GetRandom(0, len(arr)-1)]
}
