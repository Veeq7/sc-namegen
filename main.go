package main

import (
	"flag"
	"math/rand"
	"os"
	"sort"
	"time"

	"gitlab.com/Veeq7/sc-namegen/namegen"
	"gitlab.com/Veeq7/sc-namegen/utils"
)

var (
	wordCount  int
	doSort     bool
	outputFile string
)

func init() {
	flag.IntVar(&wordCount, "count", 30000, "How many names to generate")
	flag.BoolVar(&doSort, "sort", false, "Sorts the output list alphabetically")
	flag.StringVar(&outputFile, "o", "names.txt", "Output file name")
	rand.Seed(time.Now().UTC().UnixNano())
}

func main() {
	flag.Parse()

	file, err := os.Create("names.txt")
	if err != nil {
		panic(err)
	}
	strs := make([]string, 0)
	for i := 0; i < wordCount; i++ {
		name := namegen.NewZergName()
		for utils.ContainsString(strs, name) { // regenerate duplicates
			name = namegen.NewZergName()
		}
		strs = append(strs, name)
	}
	if doSort {
		sort.Strings(strs)
	}
	for i := 0; i < wordCount; i++ {
		_, err := file.WriteString(strs[i] + "\n")
		if err != nil {
			panic(err)
		}
	}
}
