package namegen

var (
	Consonants = []string{"b", "c", "d", "f", "g", "h", "j", "k", "l", "m", "n", "p", "q", "r", "s", "t", "v", "w", "x", "y", "z"}
	Vowels     = []string{"a", "e", "i", "o", "u", "y"}
)
