package namegen

import (
	"strings"

	"gitlab.com/Veeq7/sc-namegen/utils"
)

var (
	ZergCombinations = []string{"th", "st", "rv", "ct", "sz", "lk", "bl", "nv", "rg", "lk", "lm", "rr", "ck", "ntr", "sth", "rgr", "ltr", "vilis"}
	ZergSuffixes     = []string{"leth", "ling", "kor", "rokor", "lisk", "lor", "thalor"}
)

func getRandomZergCombination() string {
	name := ""
	if utils.GetRandom(0, 1) == 0 {
		name += utils.GetRandomString(Consonants)
	} else {
		name += utils.GetRandomString(ZergCombinations)
	}
	return name
}

// NewZergName creates a random zerg name
func NewZergName() string {
	name := ""
	if utils.GetRandom(0, 1) == 0 { // start with vowel randomly
		name += utils.GetRandomString(Vowels)
	}
	count := utils.GetSmoothedRandom(1, 3)
	for i := 0; i < count; i++ {
		name += getRandomZergCombination()
		name += utils.GetRandomString(Vowels)
		if utils.GetRandom(0, 16) == 0 { // small chance for double vowel
			name += utils.GetRandomString(Vowels)
		}
	}
	name = strings.Title(name)
	name += utils.GetRandomString(ZergSuffixes)

	return name
}
